#!/usr/bin/env python3
"""
take input from stdin and dump statistics accordingly.
"""
import sys
import re


def main():
    """functional core"""

    counter = int()
    values = []
    total = int()

    for counter, value in enumerate(sys.stdin):
        values.append(value)
        total += int(value)
    
    average = total / counter
    print("Entries: {0}".format(counter))
    print("Average: {0}".format(int(average)))
    print("Min: {0}".format(min(values)), end='')
    print("Max: {0}".format(max(values)), end='')


if __name__ == "__main__":
    main()